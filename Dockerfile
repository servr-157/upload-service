FROM openjdk:21-jdk

WORKDIR /app

COPY target/upload-service-0.0.1-SNAPSHOT.jar app.jar

RUN mkdir /uploads

EXPOSE 7020

ENTRYPOINT ["java", "-jar", "app.jar"]
