package com.kilo.service.upload;

import io.minio.MinioClient;
import io.minio.PutObjectArgs;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class UploadService {

    @Value("${spring.minio.base-url}")
    private String baseUrl;

    @Autowired
    private MinioClient minioClient;

    private static final String BLOG_CONTENT_FOLDER = "";

    public String upload(MultipartFile multipartFile) {
        String uniqueFileName = BLOG_CONTENT_FOLDER + multipartFile.getOriginalFilename();
        try {
            minioClient.putObject(PutObjectArgs.builder().bucket("upload-service").object(uniqueFileName).stream(multipartFile.getInputStream(), multipartFile.getSize(), -1).contentType(multipartFile.getContentType()).build());
            return baseUrl + "/" + uniqueFileName;
        } catch (Exception e) {
            throw new RuntimeException("Error occurred while uploading file to Minio", e);
        }
    }
}
