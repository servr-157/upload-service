//package com.kilo.service.upload;
//
//import lombok.RequiredArgsConstructor;
//import org.apache.commons.lang3.StringUtils;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.stereotype.Component;
//import org.springframework.web.multipart.MultipartFile;
//
//import java.time.Instant;
//import java.util.Calendar;
//import java.util.List;
//import java.util.Optional;
//
//
//@Component
//@RequiredArgsConstructor
//public class UploadUtils {
//
//    private final MinioService minioService;
//
//    @Value("${spring.minio.allowed-file-type}")
//    private List<String> allowedFileType;
//
//    @Value("${spring.minio.bucket}")
//    private String bucket;
//
//    public String upload(MultipartFile file, String path) {
//
////        if(!awss3Service.doesBucketExist(bucket))
////            throw new BadRequestException(String.format("Bucket name %s does not exist", bucket));
//
//        Optional<String> optionalExt = this.getExtensionByStringHandling(file.getOriginalFilename());
//        String fileExt;
//
//        if(optionalExt.isPresent())
//            fileExt = optionalExt.get();
//        else
//            throw new RuntimeException(String.format("Invalid file name: %s", file.getOriginalFilename()));
//
//        String fileName = String.valueOf(Instant.now().getEpochSecond()).concat(".").concat(fileExt);
//
//        if (allowedFileType != null && !allowedFileType.contains(fileExt.toUpperCase()))
//            throw new RuntimeException(String.format("File type not allow %1s, allowed files type are %2s", fileExt.toUpperCase(), allowedFileType));
//
//        minioService.putObject(bucket, path.concat(fileName), file);
//
//        return fileName;
//    }
//
//    private String getFolderName(){
//        Calendar date = Calendar.getInstance();
//        int year = date.get(Calendar.YEAR);
//        int month = date.get(Calendar.MONTH);
//        return year + "-" + StringUtils.leftPad(String.valueOf(month + 1), 2, "0");
//    }
//
//    private Optional<String> getExtensionByStringHandling(String filename) {
//        return Optional.ofNullable(filename)
//                .filter(f -> f.contains("."))
//                .map(f -> f.substring(filename.lastIndexOf(".") + 1));
//    }
//
//}
