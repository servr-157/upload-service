package com.kilo.service.upload;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Map;

@RestController
@RequestMapping("api/v1/upload")
@RequiredArgsConstructor
public class UploadController {

    private final UploadService uploadService;
    @Value("${spring.minio.base-url}")
    private String baseUrl;

    @PostMapping(consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<?> fileUpload(@RequestPart("file") MultipartFile[] files) throws IOException {
        for (MultipartFile file : files) {
            uploadService.upload(file);
        }
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }
    @PostMapping("ckeditor")
    public Map<String, String> uploadCkeditor(@RequestParam("upload") MultipartFile file) {
        return Map.of("url", baseUrl.concat("").concat(uploadService.upload(file)));
    }
}
