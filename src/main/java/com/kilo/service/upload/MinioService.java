//package com.kilo.service.upload;
//
//import io.minio.*;
//import io.minio.errors.*;
//import io.minio.messages.Bucket;
//import io.minio.messages.Item;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Component;
//import org.springframework.stereotype.Service;
//import org.springframework.web.multipart.MultipartFile;
//
//import java.io.File;
//import java.io.IOException;
//import java.security.InvalidKeyException;
//import java.security.NoSuchAlgorithmException;
//import java.util.List;
//import java.util.concurrent.ExecutionException;
//@Slf4j
//@Service
//class MinioService {
//
//    @Autowired(required = false)
//    private MinioClient minioClient;
//    private MinioAsyncClient asyncClient;
//
//    //is bucket exist?
//
//    private boolean bucketExists(String bucketName) {
//        try {
//            return minioClient.bucketExists(BucketExistsArgs.builder().bucket(bucketName).build());
//        } catch (Exception e) {
//            log.error("Error checking bucket existence: {}", e.getMessage());
//            return false;
//        }
//    }
//
//
//    // Create a bucket
//    public void createBucket(MakeBucketArgs args) {
//        try {
//            minioClient.makeBucket(args);
//        } catch (Exception e) {
//            log.error("Error creating bucket {}: {}", args.bucket(), e.getMessage());
//            throw new RuntimeException("Error creating bucket " + args.bucket(), e);
//        }
//    }
//
//
//    // List all buckets
//    public List<Bucket> listBuckets() {
//        try {
//            return minioClient.listBuckets();
//        } catch (Exception e) {
//            log.error("Error listing buckets: {}", e.getMessage());
//            throw new RuntimeException("Error listing buckets", e);
//        }
//    }
//
//
//    // Delete a bucket
//    public void deleteBucket(String bucketName) {
//        try {
//            minioClient.removeBucket(RemoveBucketArgs.builder().bucket(bucketName).build());
//        } catch (Exception e) {
//            log.error("Error deleting bucket {}: {}", bucketName, e.getMessage());
//            throw new RuntimeException("Error deleting bucket " + bucketName, e);
//        }
//    }
//
//
//    // Upload an object from File
//    public ObjectWriteResponse putObject(String bucketName, String key, File file) {
//        try {
//            return minioClient.putObject(PutObjectArgs.builder()
//                    .bucket(bucketName)
//                    .object(key)
//                    .build());
//        } catch (Exception e) {
//            log.error("Error uploading file {} to bucket {}: {}", file.getName(), bucketName, e.getMessage());
//            throw new RuntimeException("Error uploading file " + file.getName() + " to bucket " + bucketName, e);
//        }
//    }
//
//
//    // Upload an object from MultipartFile
//    public void putObject(String bucketName, String key, MultipartFile file) {
//        try {
//            PutObjectArgs putObjectArgs = PutObjectArgs.builder()
//                    .bucket(bucketName)
//                    .object(key)
//                    .stream(file.getInputStream(), file.getSize(), -1)
//                    .contentType(file.getContentType())
//                    .build();
//
//            minioClient.putObject(putObjectArgs);
//        } catch (Exception e) {
//            log.error("Error uploading file {} to bucket {}: {}", file.getOriginalFilename(), bucketName, e.getMessage());
//            throw new RuntimeException("Error while trying to upload file " + file.getOriginalFilename(), e);
//        }
//    }
//
//
//    public Iterable<Result<Item>> listObjects(String bucketName) {
//        try {
//            Iterable<Result<Item>> results = minioClient.listObjects(
//                    ListObjectsArgs.builder().bucket(bucketName).build());
//            return results;
//        } catch (Exception e) {
//            log.error("Error listing objects in bucket {}: {}", bucketName, e.getMessage());
//            throw new RuntimeException("Error listing objects in bucket " + bucketName, e);
//        }
//    }
//
//    // Get an object from MinIO
//    public GetObjectResponse getObject(String bucketName, String objectKey) {
//        try {
//            return minioClient.getObject(
//                    GetObjectArgs.builder()
//                            .bucket(bucketName)
//                            .object(objectKey)
//                            .build());
//        } catch (Exception e) {
//            log.error("Error getting object {} from bucket {}: {}", objectKey, bucketName, e.getMessage());
//            throw new RuntimeException("Error getting object " + objectKey + " from bucket " + bucketName, e);
//        }
//    }
//
//    //copying an object
//    public ObjectWriteResponse copyObject(CopyObjectArgs args) throws ErrorResponseException, InsufficientDataException, InternalException, InvalidKeyException, InvalidResponseException, IOException, NoSuchAlgorithmException, ServerException, XmlParserException {
//        try {
//            return (ObjectWriteResponse) this.asyncClient.copyObject(args).get();
//        } catch (InterruptedException var3) {
//            InterruptedException e = var3;
//            throw new RuntimeException(e);
//        } catch (ExecutionException var4) {
//            ExecutionException e = var4;
//            this.asyncClient.throwEncapsulatedException(e);
//            return null;
//        }
//    }
//
//    //deleting an object
//    public void deleteObject(String bucketName, String objectKey) {
//        try {
//            minioClient.removeObject(RemoveObjectArgs
//                    .builder()
//                    .bucket(bucketName)
//                    .object(objectKey).build());
//        } catch (Exception e) {
//            log.error("Error deleting object {} from bucket {}: {}", objectKey, bucketName, e.getMessage());
//            throw new RuntimeException("Error deleting object " + objectKey + " from bucket " + bucketName, e);
//        }
//    }
//
//    //deleting multiple Objects
//    public  DeleteObjectsResponse deleteObjects(RemoveObjectsArgs delObjReq) {
//        try {
//            minioClient.removeObjects(delObjReq);
//        } catch (Exception e) {
//            log.error("Error deleting objects from bucket {}: {}", delObjReq.bucket(), e.getMessage());
//            throw new RuntimeException("Error deleting objects from bucket " + delObjReq.bucket(), e);
//        }
//        return (deleteObjects(delObjReq));
//    }
//
//}
